import tkinter
from tkinter.filedialog import askdirectory


class WindowSettings(tkinter.Tk):
    def __init__(self, parent=None):
        self.dir_db = None

        tkinter.Tk.__init__(self, parent)
        self.parent = parent
        self.title('KeyMaker')

        self.grid()
        root = tkinter.LabelFrame(self, text='Settings')
        root.grid(row=0, columnspan=7, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)

        btn_select_dir_db = tkinter.Button(root, text='Select DB Directory', command=self.select_dir_db)
        btn_select_dir_db.grid(row=4, column=3, sticky='W', padx=5, pady=2)

    def select_dir_db(self):
        self.dir_db = askdirectory()

        self.withdraw()
        self.quit()
