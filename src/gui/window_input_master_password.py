import tkinter


class WindowInputMasterPassword(tkinter.Tk):
    def __init__(self, parent=None):
        self.val_full_name = None
        self.val_master_password = None

        tkinter.Tk.__init__(self, parent)
        self.parent = parent
        self.title('KeyMaker')

        self.grid()
        root = tkinter.LabelFrame(self, text='Generate Master Key')
        root.grid(row=0, columnspan=7, sticky='W', padx=5, pady=5, ipadx=5, ipady=5)
        self.Val1Lbl = tkinter.Label(root, text='Full Name')
        self.Val1Lbl.grid(row=0, column=0, sticky='E', padx=5, pady=2)
        self.Val1Txt = tkinter.Entry(root)
        self.Val1Txt.grid(row=0, column=1, columnspan=3, pady=2, sticky='WE')
        self.Val2Lbl = tkinter.Label(root, text='Master Password')
        self.Val2Lbl.grid(row=1, column=0, sticky='E', padx=5, pady=2)
        self.Val2Txt = tkinter.Entry(root, show="*")
        self.Val2Txt.grid(row=1, column=1, columnspan=3, pady=2, sticky='WE')

        cancel_btn = tkinter.Button(root, text='Cancel', command=self.cancel)
        cancel_btn.grid(row=4, column=1, sticky='W', padx=5, pady=2)
        submit_btn = tkinter.Button(root, text='Submit', command=self.submit)
        submit_btn.grid(row=4, column=3, sticky='W', padx=5, pady=2)

    def cancel(self):
        self.withdraw()
        self.destroy()

    def submit(self):
        self.val_full_name = self.Val1Txt.get()
        if self.val_full_name == '':
            Win2 = tkinter.Tk()
            Win2.withdraw()

        self.val_master_password = self.Val2Txt.get()
        if self.val_master_password == '':
            Win2 = tkinter.Tk()
            Win2.withdraw()

        self.withdraw()
        self.quit()
