import json

from config import Config


class DbIo:
    def __init__(self):
        self.config = Config()

    def load_db(self):
        file_path_db = self.config.configparser['db']['file_path']
        with open(file_path_db, 'r', encoding='utf-8') as file_db:
            return json.load(file_db, encoding='utf-8')

    def save_db(self, db):
        file_path_db = self.config.configparser['db']['file_path']
        with open(file_path_db, 'w', encoding='utf-8') as file_db:
            json.dump(db, file_db, ensure_ascii=False, sort_keys=True, indent=2)
