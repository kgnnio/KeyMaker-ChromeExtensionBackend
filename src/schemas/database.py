import jsl

from schemas.account import Account as AccountSchema


class Database(jsl.Document):
    nextId = jsl.IntField(minimum=0, required=True)
    accounts = jsl.ArrayField(jsl.OneOfField([jsl.DocumentField(AccountSchema, as_ref=True)]), min_items=0, unique_items=True)
