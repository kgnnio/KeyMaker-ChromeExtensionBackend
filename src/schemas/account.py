import jsl


class Account(jsl.Document):
    id = jsl.IntField(minimum=0, required=True)
    name = jsl.StringField(required=True)
    user = jsl.StringField(required=True)
    uri = jsl.UriField(required=True)
    passwordType = jsl.IntField(minimum=0, maximum=8, required=True)
    counter = jsl.IntField(minimum=1, maximum=4294967295, required=True)
