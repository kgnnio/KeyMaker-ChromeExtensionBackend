import jsl

from schemas.account import Account as AccountSchema


class CommandResult(jsl.Document):
    success = jsl.BooleanField(required=True)
    errorMessage = jsl.StringField(required=False)


class CreateAccount(CommandResult):
    command = jsl.StringField(pattern='^CreateAccount$', required=True)
    id = jsl.IntField(minimum=0, required=False)


class UpdateAccount(CommandResult):
    command = jsl.StringField(pattern='^UpdateAccount$', required=True)


class DeleteAccount(CommandResult):
    command = jsl.StringField(pattern='^DeleteAccount$', required=True)


class DeleteAccounts(CommandResult):
    command = jsl.StringField(pattern='^DeleteAccounts$', required=True)


class GetAccounts(CommandResult):
    command = jsl.StringField(pattern='^GetAccounts$', required=True)
    accounts = jsl.ArrayField(jsl.OneOfField([jsl.DocumentField(AccountSchema, as_ref=True)]), min_items=0, unique_items=True, required=False)


class IsAccountExist(CommandResult):
    command = jsl.StringField(pattern='^IsAccountExist', required=True)
    ids = jsl.ArrayField(jsl.IntField(), unique_items=True, required=False)
    exists = jsl.BooleanField(required=True)


class RequestPassword(CommandResult):
    command = jsl.StringField(pattern='^RequestPassword$', required=True)
    password = jsl.StringField(required=True)


class InputMasterPassword(CommandResult):
    command = jsl.StringField(pattern='^InputMasterPassword$', required=True)


class IsMasterKeyExist(CommandResult):
    command = jsl.StringField(pattern='^IsMasterKeyExist$', required=True)
    exists = jsl.BooleanField(required=True)


class DeleteMasterKey(CommandResult):
    command = jsl.StringField(pattern='^DeleteMasterKey$', required=True)


class CopyPasswordToClipboard(CommandResult):
    command = jsl.StringField(pattern='^CopyPasswordToClipboard$', required=True)


class SetSettings(CommandResult):
    command = jsl.StringField(pattern='^SetSettings$', required=True)
