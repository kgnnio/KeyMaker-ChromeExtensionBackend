import jsl


class CreateAccount(jsl.Document):
    name = jsl.StringField(required=True)
    user = jsl.StringField(required=True)
    uri = jsl.UriField(required=True)
    passwordType = jsl.IntField(minimum=0, maximum=8, required=True)
    counter = jsl.IntField(minimum=1, maximum=4294967295, required=True)


class UpdateAccount(jsl.Document):
    id = jsl.IntField(required=True)
    name = jsl.StringField(required=False)
    user = jsl.StringField(required=False)
    uri = jsl.UriField(required=False)
    passwordType = jsl.IntField(minimum=0, maximum=8, required=False)
    counter = jsl.IntField(minimum=1, maximum=4294967295, required=False)


class DeleteAccount(jsl.Document):
    id = jsl.IntField(required=True)


class DeleteAccounts(jsl.Document):
    ids = jsl.ArrayField(jsl.IntField(), min_items=1, unique_items=True)


class GetAccounts(jsl.Document):
    pass


class IsAccountExist(jsl.Document):
    uri = jsl.UriField(required=True)


class RequestPassword(jsl.Document):
    id = jsl.IntField(required=True)


class InputMasterPassword(jsl.Document):
    pass


class IsMasterKeyExist(jsl.Document):
    pass


class DeleteMasterKey(jsl.Document):
    pass


class CopyPasswordToClipboard(jsl.Document):
    id = jsl.IntField(required=True)


class SetSettings(jsl.Document):
    pass
