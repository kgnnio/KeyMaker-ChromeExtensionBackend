class Helper:
    @staticmethod
    def get_first_key(json) -> str:
        for key in json.keys():
            first_key = key
            return first_key

    @staticmethod
    def get_first_content(json) -> dict:
        try:
            first_content = json[0]

        except IndexError:
            first_content = {}

        return first_content
