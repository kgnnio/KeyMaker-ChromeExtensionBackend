import os
import shutil
import tkinter

import pyperclip
from jsonschema import validate

import log as l
from config import Config
from credential_container import CredentialContainer
from db_io import DbIo
from gui.window_input_master_password import WindowInputMasterPassword
from gui.window_settings import WindowSettings
from native_messaging_api.command_implementation_base import CommandImplementationBase
from schemas.account import Account as AccountSchema
from schemas.database import Database as DatabaseSchema

l = l.setup(os.path.basename(__file__))

db_io = DbIo()
master_key_container = CredentialContainer()

class CreateAccount(CommandImplementationBase):
    def _execute(self) -> None:
        db = db_io.load_db()

        account = self.command_args
        account['id'] = db['nextId']
        db['nextId'] = account['id'] + 1
        validate(account, AccountSchema.get_schema())

        db['accounts'].append(account)
        validate(db, DatabaseSchema.get_schema())

        db_io.save_db(db)

        self.command_result = dict()
        self.command_result['id'] = account['id']
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class UpdateAccount(CommandImplementationBase):
    def _execute(self) -> None:
        account_id = self.command_args['id']
        db = db_io.load_db()

        accounts = db['accounts']
        account_to_update = None
        for account in accounts:
            if account['id'] == account_id:
                account_to_update = account
                break

        if account_to_update is None:
            raise IndexError

        if 'name' in self.command_args:
            account_to_update['name'] = self.command_args['name']
        if 'user' in self.command_args:
            account_to_update['user'] = self.command_args['user']
        if 'uri' in self.command_args:
            account_to_update['uri'] = self.command_args['uri']
        if 'passwordType' in self.command_args:
            account_to_update['passwordType'] = self.command_args['passwordType']
        if 'counter' in self.command_args:
            account_to_update['counter'] = self.command_args['counter']

        db_io.save_db(db)

        self.command_result = dict()
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class DeleteAccount(CommandImplementationBase):
    def _execute(self) -> None:
        account_to_delete_id = self.command_args['id']

        db = db_io.load_db()

        accounts = db['accounts']
        for account_id, account in enumerate(accounts):
            if account['id'] == account_to_delete_id:
                del accounts[account_id]
                break

        db_io.save_db(db)

        self.command_result = dict()
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class DeleteAccounts(CommandImplementationBase):
    def _execute(self) -> None:
        accounts_to_delete_ids = self.command_args['ids']

        db = db_io.load_db()

        accounts = db['accounts']
        accounts[:] = [d for d in accounts if d.get('id') not in accounts_to_delete_ids]

        db_io.save_db(db)

        self.command_result = dict()
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class GetAccounts(CommandImplementationBase):
    def _execute(self) -> None:
        db = db_io.load_db()

        self.command_result = dict()
        self.command_result['accounts'] = db['accounts']
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class IsAccountExist(CommandImplementationBase):
    def _execute(self) -> None:
        account_to_find_uri = self.command_args['uri']
        account_ids = []
        account_exists = False

        db = db_io.load_db()

        accounts = db['accounts']
        for account in accounts:
            l.debug('account[uri]: ' + account['uri'])
            if account['uri'] == account_to_find_uri:
                account_ids.append(account['id'])
                account_exists = True

        l.debug('account_ids: ' + str(account_ids))
        db_io.save_db(db)

        self.command_result['command'] = self.__class__.__name__
        if account_ids:
            self.command_result['ids'] = account_ids
        self.command_result['exists'] = account_exists
        self.command_result['success'] = True


class RequestPassword(CommandImplementationBase):
    def _execute(self) -> None:
        account_password_requested_id = self.command_args['id']
        account_password_requested = None

        db = db_io.load_db()

        accounts = db['accounts']
        for account in accounts:
            if account['id'] == account_password_requested_id:
                account_password_requested = account
                break

        account_password = None
        if account_password_requested is not None:
            account_password = master_key_container.generate_password(account_password_requested)

        self.command_result['password'] = account_password
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class InputMasterPassword(CommandImplementationBase):
    def _execute(self) -> None:
        window_input_master_password = WindowInputMasterPassword()
        window_input_master_password.mainloop()

        try:
            window_input_master_password.destroy()
            l.debug(window_input_master_password.val_full_name)
            l.debug(window_input_master_password.val_master_password)

            master_key_container.contain_credentials(window_input_master_password.val_full_name,
                                                     window_input_master_password.val_master_password)

        except tkinter.TclError:
            l.debug('Operation has been cancelled.')

        except Exception as exception:
            l.debug('Error: %s, %s', exception.__class__.__name__, exception)

        self.command_result = dict()
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class IsMasterKeyExist(CommandImplementationBase):
    def _execute(self) -> None:
        master_key_exists = master_key_container.is_contain_credentials()

        self.command_result = dict()
        self.command_result['exists'] = master_key_exists
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class DeleteMasterKey(CommandImplementationBase):
    def _execute(self) -> None:
        master_key_container.delete_credentials()

        self.command_result = dict()
        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class CopyPasswordToClipboard(CommandImplementationBase):
    def _execute(self) -> None:
        account_password_requested_id = self.command_args['id']
        account_password_requested = None

        db = db_io.load_db()

        accounts = db['accounts']
        for account in accounts:
            if account['id'] == account_password_requested_id:
                account_password_requested = account
                break

        if account_password_requested is not None:
            account_password = master_key_container.generate_password(account_password_requested)

            pyperclip.copy(account_password)
            if not pyperclip.copy:
                l.debug("Copy functionality unavailable.")

        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True


class SetSettings(CommandImplementationBase):
    def _execute(self) -> None:
        window_settings = WindowSettings()
        window_settings.mainloop()

        try:
            l.debug(window_settings.dir_db)
            window_settings.destroy()

            config = Config()
            current_db = config.get('db', 'file_path')
            shutil.copyfile(current_db, window_settings.dir_db + '/keymaker_db.json')
            config.set('db', 'file_path', window_settings.dir_db + '/keymaker_db.json')

            os.remove(current_db)

        except tkinter.TclError:
            l.debug('Operation has been cancelled.')

        except Exception as exception:
            l.debug('Error: %s, %s', exception.__class__.__name__, exception)

        self.command_result['command'] = self.__class__.__name__
        self.command_result['success'] = True
