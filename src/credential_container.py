import mpw

password_type = {0: 'max', 1: 'long', 2: 'medium', 3: 'basic', 4: 'short', 5: 'pin', 6: 'max_no_symbols', 7: 'long_no_symbols', 8: 'medium_no_symbols'}


class CredentialContainer:
    def __init__(self):
        self.full_name = None
        self.master_password = None
        self.master_key = None

    def contain_credentials(self, full_name, master_password):
        self.full_name = full_name
        self.master_password = master_password
        self.master_key = mpw.get_master_key(master_password, full_name, 'v3')

    def is_contain_credentials(self):
        return bool(self.master_key)

    def delete_credentials(self):
        self.master_key = None

    def generate_password(self, account):
        account_name = account['name']
        account_password_type = password_type[account['passwordType']]
        account_counter = account['counter']
        master_key = self.master_key

        account_password = mpw.get_password(master_key, account_name,
                                            counter=account_counter,
                                            pwd_type=mpw.PwdType(account_password_type),
                                            variant=mpw.Scope.password,
                                            context='',
                                            version='v3')

        return account_password
