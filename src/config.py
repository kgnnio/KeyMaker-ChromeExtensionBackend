import configparser
import os
import shutil

import externals


class Config:
    def __init__(self):
        self.dir_config = externals.dir_config
        self.file_path_config = externals.file_path_config
        self.file_path_config_init = externals.file_path_config_init
        self.file_path_db = externals.file_path_db
        self.file_path_db_init = externals.file_path_db_init

        self.configparser = configparser.ConfigParser()
        self.configparser.read(self.file_path_config)

        self._create_new_config_dir_and_files_if_not_exist()
        self._update_db_file_path_if_not_exist()

    def _create_new_config_dir_and_files_if_not_exist(self):
        if not os.path.exists(self.dir_config):
            os.makedirs(self.dir_config)
            shutil.copyfile(self.file_path_config_init, self.file_path_config)
            shutil.copyfile(self.file_path_db_init, self.file_path_db)

    def _update_db_file_path_if_not_exist(self):
        db = self.configparser['db']
        file_path_db = db['file_path']
        if len(file_path_db) == 0:
            self.configparser['db']['file_path'] = self.file_path_db
            self._save_current_config()

        file_exists = os.path.isfile(self.file_path_db)
        if not file_exists:
            shutil.copyfile(self.file_path_db_init, self.file_path_db)

    def _save_current_config(self) -> None:
        with open(self.file_path_config, 'w') as file_config:
            self.configparser.write(file_config)

    def get(self, section, option):
        section = self.configparser[section]
        option_content = section[option]

        return option_content

    def set(self, section, option, value):
        self.configparser[section][option] = value
        self._save_current_config()
