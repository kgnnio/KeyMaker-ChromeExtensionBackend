import logging

from config import Config

config = Config()


def setup(name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    logging.basicConfig(level=config.get('logging', 'level'))

    handler = logging.FileHandler('keymaker.log')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(config.get('logging', 'level'))
    logger.addHandler(handler)

    return logger
