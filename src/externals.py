import os

dir_config = os.path.join(os.path.expanduser('~') + '/.config/keymaker/')
file_path_config = os.path.join(dir_config, 'config.ini')
file_path_config_init = os.path.abspath('./inits/config_init.ini')
file_path_db = os.path.join(dir_config, 'keymaker_db.json')
file_path_db_init = os.path.abspath('./inits/db_init.json')
