import inspect
import json
import os

import commands
import log as l

from helper import Helper

l = l.setup(os.path.basename(__file__))


class CommandDispatcher:
    def __init__(self):
        self.command_implementations = dict()
        self._get_command_implementations()

    def do(self, command_and_args: str) -> str:
        command_and_args = json.loads(command_and_args)
        command_name = Helper.get_first_key(command_and_args)
        command_implementation = self.command_implementations[command_name]
        command_to_execute = command_implementation(**command_and_args)

        command_result = json.dumps(command_to_execute.execute(), ensure_ascii=False, sort_keys=True)
        return command_result

    def _get_command_implementations(self) -> None:
        for command_name, command_implementation in inspect.getmembers(commands, inspect.isclass):
            if command_implementation.__module__ == commands.__name__:
                self.command_implementations[command_name] = command_implementation
