import dummy_threading as threading
import os
import struct
import sys

import log as l

l = l.setup(os.path.basename(__file__))


class ListenerResponderDaemon(threading.Thread):
    def __init__(self, message_responder):
        threading.Thread.__init__(self)
        self.daemon = True

        self.message_responder = message_responder

        self.message = str()
        self.message_response = str()

    def run(self):
        l.debug("%s thread started." % self.getName())
        self.listen_for_messages()

    def listen_for_messages(self):
        try:
            message_length_bytes = sys.stdin.buffer.read(4)
            message_length = struct.unpack('i', message_length_bytes)[0]
            self.message = sys.stdin.buffer.read(message_length).decode('utf-8')

            l.debug('Received message: ' + str(self.message))

        except Exception as exception:
            l.debug('Error occurred: ' + str(exception))
            l.exception('Error occurred: ')

        self.construct_message_response()

    def construct_message_response(self):
        l.debug('Constructing message response: ' + str(self.message))
        self.message_response = self.message_responder.do(self.message)
        self.send_message_response()

    def send_message_response(self):
        l.debug('Sending message: ' + self.message_response)

        try:
            message_encoded = self.message_response.encode('utf-8')
            sys.stdout.buffer.write(struct.pack('I', len(message_encoded)))
            sys.stdout.buffer.write(message_encoded)
            sys.stdout.buffer.flush()

        except Exception as exception:
            l.debug('Error: %s, %s', exception.__class__.__name__, exception)
            l.exception('Error occurred: ')
