import inspect
import os

import log as l
import schemas.commands as command_schemas
from jsonschema import validate

import schemas.command_results as command_result_schemas
from helper import Helper

l = l.setup(os.path.basename(__file__))


class CommandImplementationBase:
    def __init__(self, **kwargs):
        self.command_and_args = kwargs
        self.command_name = Helper.get_first_key(self.command_and_args)
        self.command_args = Helper.get_first_content(self.command_and_args[self.command_name])
        self.command_result = dict()

    def execute(self):
        if self._is_command_as_defined_in_command_schema():
            try:
                self._execute()
                l.debug('Command executed: ' + str(self.command_and_args))

            except Exception as exception:
                l.debug('Error: %s, %s', exception.__class__.__name__, exception)
                l.exception('Error occurred: ')

        else:
            self._make_command_result_failed('Command was not valid.')

        if not self._is_command_result_as_defined_in_schema():
            self._make_command_result_failed('Command results was not valid.')

        return self.command_result

    def _is_command_as_defined_in_command_schema(self) -> bool:
        try:
            all_command_schemas = inspect.getmembers(command_schemas, inspect.isclass)
            command_schema = all_command_schemas[[i[0] for i in all_command_schemas].index(self.command_name)][1].get_schema()

            validate(self.command_args, command_schema)
            l.debug('Command arguments successfully validated against ' + self.command_name + ' schema.')

            return True

        except KeyError:
            l.debug('Command ' + self.command_name + ' does not exist in schema.')

            return False

        except Exception as exception:
            l.debug('Error: %s, %s', exception.__class__.__name__, exception)
            l.exception('Error occurred: ')
            return False

    def _execute(self):
        pass

    def _is_command_result_as_defined_in_schema(self):
        try:
            all_command_result_schemas = inspect.getmembers(command_result_schemas, inspect.isclass)
            command_result_schema = all_command_result_schemas[[i[0] for i in all_command_result_schemas].index(self.command_name)][1].get_schema()

            validate(self.command_result, command_result_schema)
            l.debug('Command results successfully validated against ' + str(self.command_name) + ' results schema.')

            return True

        except KeyError:
            l.debug('Command result' + self.command_name + ' does not exist in schema.')

            return False

        except Exception as exception:
            l.debug('Error: %s, %s', exception.__class__.__name__, exception)

    def _make_command_result_failed(self, error_message):
        self.command_result = dict()
        self.command_result['success'] = False
        self.command_result['errorMessage'] = error_message
