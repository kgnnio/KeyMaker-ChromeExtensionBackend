#!/usr/bin/env python3

import os
import time

import log as l
from native_messaging_api.command_dispatcher import CommandDispatcher
from native_messaging_api.listener_responder_daemon import ListenerResponderDaemon

l = l.setup(os.path.basename(__file__))

if __name__ == '__main__':
    daemon = ListenerResponderDaemon(CommandDispatcher())
    daemon.start()

    while daemon.is_alive():
        daemon.listen_for_messages()
        time.sleep(.1)
