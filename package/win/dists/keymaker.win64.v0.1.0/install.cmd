@echo off &setlocal

set DIR_INSTALL=%~dp0
set FILE_HOST=%DIR_INSTALL%pw.keymaker.json
set FILE_EXE=%DIR_INSTALL%keymaker.exe

REG ADD "HKCU\Software\Google\Chrome\NativeMessagingHosts\pw.keymaker" /ve /t REG_SZ /d %FILE_HOST% /f

set "search=__PATH__"
set "replace=%FILE_EXE:\=\\%"
set "FILE_HOST_TEMPLATE=.\templates\pw.keymaker.json"
set "FILE_HOST_UPDATED=pw.keymaker.json"
(for /f "delims=" %%i in (%FILE_HOST_TEMPLATE%) do (
    set "line=%%i"
    setlocal enabledelayedexpansion
    set "line=!line:%search%=%replace%!"
    echo(!line!
    endlocal
))>"%FILE_HOST_UPDATED%"
