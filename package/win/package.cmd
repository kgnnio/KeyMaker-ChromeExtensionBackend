@echo off &setlocal

set "VERSION=0.1.0"
set "NAME_PACKAGE=keymaker.win64.v%VERSION%"

pyinstaller --hidden-import=_scrypt --hidden-import=mpw.v3 --hidden-import=log --onefile --clean ../../keymaker.py

if not exist ".\dists" mkdir ".\dists"
if not exist ".\dists\%NAME_PACKAGE%" mkdir ".\dists\%NAME_PACKAGE%"
if not exist ".\dists\%NAME_PACKAGE%\templates" mkdir ".\dists\%NAME_PACKAGE%\templates"

cp ".\dist\keymaker.exe" ".\dists\%NAME_PACKAGE%\keymaker.exe"
cp ".\accessory_files\install.cmd" ".\dists\%NAME_PACKAGE%\install.cmd"
cp "..\pw.keymaker.json" ".\dists\%NAME_PACKAGE%\templates\pw.keymaker.json"
cp "..\..\db_init.json" ".\dists\%NAME_PACKAGE%\db_init.json"
cp "..\..\config_init.ini" ".\dists\%NAME_PACKAGE%\config_init.ini"

rm keymaker.spec
rd /s /q build
rd /s /q dist