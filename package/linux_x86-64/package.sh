#!/usr/bin/env bash

# Tested in the following environment
# Ubuntu 16.04 (Xenial)
# Python: 3.5.2
# PyInstaller: 3.2

source ../version.sh
printf "Building as version %s\n" "${VERSION}"

DIR_ACCESSORY_FILES="./accessory_files/"
DIR_DIST_TMP="./dist/"
DIR_DIST_UPDATED="./keymaker.$(basename $(pwd)).v${VERSION}/"
DIR_DIST_UPDATED_INITS="${DIR_DIST_UPDATED}inits/"
DIR_DISTS="./dists/"

pyinstaller --hidden-import=_scrypt --hidden-import=mpw.v3 --hidden-import=log --onefile --clean ../../src/main.py
mv ./dist/main ./dist/keymaker
chmod 700 ./dist/keymaker

cp "${DIR_ACCESSORY_FILES}"install.sh ./dist/install.sh
chmod 700 "${DIR_DIST_TMP}"install.sh
cp ../pw.keymaker.json ./dist/pw.keymaker.json
chmod 666 "${DIR_DIST_TMP}"pw.keymaker.json

mv "${DIR_DIST_TMP}" "${DIR_DIST_UPDATED}"
mkdir -p "${DIR_DIST_UPDATED_INITS}"
echo "${DIR_DIST_UPDATED_INITS}"
cp ../../src/inits/db_init.json "${DIR_DIST_UPDATED_INITS}"
cp ../../src/inits/config_init.ini "${DIR_DIST_UPDATED_INITS}"
zip -r "${DIR_DISTS}"/keymaker.$(basename $(pwd)).v${VERSION}.zip "${DIR_DIST_UPDATED}"*

rm ./main.spec
rm -r ./build/
rm -r "${DIR_DIST_UPDATED}"
