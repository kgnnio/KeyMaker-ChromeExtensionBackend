#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

APP_NAME="pw.keymaker"
HOST_FILE="$__dir/keymaker"

if [ "$(whoami)" == "root" ]; then
  printf "Do not run as root."
  exit 1
fi

# Find target dirs for various browsers & OS'es
# https://developer.chrome.com/extensions/nativeMessaging#native-messaging-host-location
# https://wiki.mozilla.org/WebExtensions/Native_Messaging
if [ $(uname -s) == 'Darwin' ]; then
  HOST_FILE="$__dir/keymaker-darwinx64"
  TARGET_DIR_CHROME="$HOME/Library/Application Support/Google/Chrome/NativeMessagingHosts"
  TARGET_DIR_CHROMIUM="$HOME/Library/Application Support/Chromium/NativeMessagingHosts"
else
  HOST_FILE="$__dir/keymaker-linux64"
  TARGET_DIR_CHROME="$HOME/.config/google-chrome/NativeMessagingHosts"
  TARGET_DIR_CHROMIUM="$HOME/.config/chromium/NativeMessagingHosts/"
fi

if [ -e "$__dir/keymaker" ]; then
  printf "Detected development binary\n"
  HOST_FILE="$__dir/keymaker"
fi

# Escape host file
ESCAPED_HOST_FILE=${HOST_FILE////\\/}

printf "\n"
printf "Select your browser:\n"
printf "====================\n"
printf "1) Chrome\n"
printf "2) Chromium\n"
echo -n "1-2: "
read BROWSER
printf "\n"

# Set target dir from user input
if [[ "$BROWSER" == "1" ]]; then
  TARGET_DIR="$TARGET_DIR_CHROME"
fi

if [[ "$BROWSER" == "2" ]]; then
  TARGET_DIR="$TARGET_DIR_CHROMIUM"
fi


# Create config dir if not existing
mkdir -p "$TARGET_DIR"

if [ "$BROWSER" == "1" ] || [ "$BROWSER" == "2" ]; then
  printf "Installing Chrome / Chromium host config"
  cp "$__dir/pw.keymaker.json" "$TARGET_DIR/$APP_NAME.json"
fi

# Replace path to host
sed -i -e "s/__PATH__/$ESCAPED_HOST_FILE/" "$TARGET_DIR/$APP_NAME.json"

# Set permissions for the manifest so that all users can read it.
chmod o+r "$TARGET_DIR/$APP_NAME.json"

printf "Native messaging host has been installed to $TARGET_DIR.\n"
