import json
import os
import shutil
import pytest

from db_io import DbIo
from config import Config


def init_mock_db():
    shutil.copyfile(os.path.abspath('../src/inits/db_init.json'), os.path.join('./mock_data/keymaker_db.json'))


@pytest.fixture(autouse=True)
def commands(monkeypatch):
    import config
    dir_config = os.path.join('./mock_data/')
    file_path_config = os.path.join(dir_config, 'config.ini')
    monkeypatch.setattr(config.externals, 'dir_config', dir_config)
    monkeypatch.setattr(config.externals, 'file_path_config', file_path_config)

    import commands
    db_io = DbIo()
    monkeypatch.setattr(commands, 'db_io', db_io)

    config = Config()
    print(config.file_path_config)
    print(config.get('logging', 'level'))
    monkeypatch.setattr(commands, 'db_io', db_io)

    return commands


def test_get_accounts(commands):
    init_mock_db()

    command_results = commands.GetAccounts(**json.loads('{"GetAccounts": []}')).execute()
    assert command_results['command'] == 'GetAccounts'
    assert command_results['success'] is True
    assert command_results['accounts'][0]['id'] == 0
    assert command_results['accounts'][0]['name'] == 'example.com'
    assert command_results['accounts'][0]['user'] == 'myusername'
    assert command_results['accounts'][0]['uri'] == 'www.example.com'
    assert command_results['accounts'][0]['passwordType'] == 1


def test_create_account(commands):
    init_mock_db()

    command_results = commands.CreateAccount(**json.loads('{"CreateAccount":[{"name":"test1","user":"user","uri":"www.test1.com","passwordType":0,"counter":1}]}')).execute()
    assert command_results['command'] == 'CreateAccount'
    assert command_results['success'] is True
    assert command_results['id'] == 1

    command_results = commands.CreateAccount(**json.loads('{"CreateAccount":[{"name":"test2","user":"user","uri":"www.test2.com","passwordType":0,"counter":1}]}')).execute()
    assert command_results['command'] == 'CreateAccount'
    assert command_results['success'] is True
    assert command_results['id'] == 2


def test_update_account(commands):
    init_mock_db()

    command_results = commands.UpdateAccount(**json.loads('{"UpdateAccount": [{"id":0,"name":"Updated","user":"Updated User","uri":"www.updated.com","passwordType":0,"counter":2}]}')).execute()
    assert command_results['command'] == 'UpdateAccount'
    assert command_results['success'] is True

    command_results = commands.GetAccounts(**json.loads('{"GetAccounts": []}')).execute()
    assert command_results['command'] == 'GetAccounts'
    assert command_results['success'] is True
    assert command_results['accounts'][0]['id'] == 0
    assert command_results['accounts'][0]['name'] == 'Updated'
    assert command_results['accounts'][0]['user'] == 'Updated User'
    assert command_results['accounts'][0]['uri'] == 'www.updated.com'
    assert command_results['accounts'][0]['passwordType'] == 0


def test_delete_account(commands):
    init_mock_db()

    command_results = commands.DeleteAccount(**json.loads('{"DeleteAccount":[{"id":0}]}')).execute()
    assert command_results['command'] == 'DeleteAccount'
    assert command_results['success'] is True

    command_results = commands.GetAccounts(**json.loads('{"GetAccounts": []}')).execute()
    assert command_results['command'] == 'GetAccounts'
    assert command_results['success'] is True
    assert len(command_results['accounts']) == 0


def test_is_account_exist(commands):
    init_mock_db()

    command_results = commands.IsAccountExist(**json.loads('{"IsAccountExist": [{"uri": "www.example.com"}]}')).execute()
    assert command_results['command'] == 'IsAccountExist'
    assert command_results['success'] is True
    assert command_results['exists'] is True

    command_results = commands.IsAccountExist(**json.loads('{"IsAccountExist": [{"uri": "www.idontexist.com"}]}')).execute()
    assert command_results['command'] == 'IsAccountExist'
    assert command_results['success'] is True
    assert command_results['exists'] is False

